package pl.inpar.builder;

/**
 * @author drackowski
 */
public @interface StepByStepFluentBuilder {
    Class<?> value() default Object.class;
    String[] skipAsNull() default {};
}
