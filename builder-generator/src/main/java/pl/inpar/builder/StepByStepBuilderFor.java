package pl.inpar.builder;

public @interface StepByStepBuilderFor {
    Class<?> value();
}
