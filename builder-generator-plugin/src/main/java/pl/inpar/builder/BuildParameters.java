package pl.inpar.builder;

/**
 * Plugin configuration
 *
 * @author drackowski
 */
public class BuildParameters {

    public enum SetterType {
        FIELD_NAME,
        SETTER,
        WITH
    }

    private SetterType setterType = SetterType.FIELD_NAME;

    private String builderClassSuffix = "FluentBuilder";

    private String staticStarterMethod = "start";

    public BuildParameters() {
    }

    public BuildParameters(SetterType setterType, String builderClassSuffix, String staticStarterMethod) {
        this.setterType = setterType;
        this.builderClassSuffix = builderClassSuffix;
        this.staticStarterMethod = staticStarterMethod;
    }

    public SetterType getSetterType() {
        return setterType;
    }

    public void setSetterType(SetterType setterType) {
        this.setterType = setterType;
    }

    public String getBuilderClassSuffix() {
        return builderClassSuffix;
    }

    public void setBuilderClassSuffix(String builderClassSuffix) {
        this.builderClassSuffix = builderClassSuffix;
    }

    public String getStaticStarterMethod() {
        return staticStarterMethod;
    }

    public void setStaticStarterMethod(String staticStarterMethod) {
        this.staticStarterMethod = staticStarterMethod;
    }

}