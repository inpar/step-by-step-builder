package pl.inpar.builder.generator.javaparser.processor.impl;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Modifier.Keyword;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.RecordDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.ArrayInitializerExpr;
import com.github.javaparser.ast.expr.ClassExpr;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.Name;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.printer.DefaultPrettyPrinter;
import pl.inpar.builder.BuildParameters;
import pl.inpar.builder.generator.javaparser.model.Constructor;
import pl.inpar.builder.generator.javaparser.model.InterfaceDescription;
import pl.inpar.builder.generator.javaparser.model.StepDescription;
import pl.inpar.builder.generator.javaparser.processor.FileProcessor;
import pl.inpar.builder.generator.javaparser.processor.ParameterWithTypeToStepClassName;
import pl.inpar.builder.generator.javaparser.processor.VariantsProcessor;
import pl.inpar.builder.generator.javaparser.processor.variants.LombokConstructorProcessorImpl;
import pl.inpar.builder.generator.javaparser.processor.variants.MultiVariantProcessorImpl;
import pl.inpar.builder.generator.javaparser.processor.variants.RecordProcessorImpl;
import pl.inpar.builder.generator.javaparser.processor.variants.StandardConstructorProcessorImpl;
import pl.inpar.builder.StepByStepBuilderFor;
import pl.inpar.builder.StepByStepFluentBuilder;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 * @author drackowski
 */
public class FileProcessorImpl implements FileProcessor {

    public static final String FIRST_STEP_INTERFACE = "FirstStep";
    private final BuildParameters buildParameters;
    private final File sourceFile;
    private final File generatedSourcesDir;
    private final CompilationUnit cu;

    private final VariantsProcessor variantsProcessor;
    private final InterfacesGeneratorImpl interfacesGenerator;
    private final StepsClassGeneratorImpl stepsClassGenerator;
    private final MainGeneratorImpl mainGenerator;
    private final Optional<StepByStepFluentBuilder> stepByStepFluentBuilderAnnotation;

    public FileProcessorImpl(BuildParameters buildParameters, File sourceFile, File generatedSourcesDir) throws IOException {
        this.buildParameters = buildParameters;
        this.sourceFile = sourceFile;
        this.generatedSourcesDir = generatedSourcesDir;

        ParameterWithTypeToStepClassName parameterWithTypeToStepClassName = new ParameterWithTypeToStepClassNameImpl();


        this.cu = new JavaParser().parse(this.sourceFile).getResult().orElseThrow(() -> new RuntimeException("Parsing input file error"));

        stepByStepFluentBuilderAnnotation = getStepByStepFluentBuilderIfExists(this.cu);

        this.variantsProcessor = new MultiVariantProcessorImpl(
            new StandardConstructorProcessorImpl(cu),
            new LombokConstructorProcessorImpl(cu),
            new RecordProcessorImpl(cu)
        );
        this.interfacesGenerator = new InterfacesGeneratorImpl(buildParameters.getSetterType(), parameterWithTypeToStepClassName);
        this.stepsClassGenerator = new StepsClassGeneratorImpl();
        this.mainGenerator = new MainGeneratorImpl(buildParameters);
    }

    @Override
    public boolean process() {
        boolean[] processed = new boolean[]{false};
        if (stepByStepFluentBuilderAnnotation.isPresent()) {
            List<Constructor> constructors = variantsProcessor.getBuilderVariants();
            for (Constructor constructor : constructors) {
                Optional<CompilationUnit> cuBuilderOptional = process(constructor);
                cuBuilderOptional.ifPresent(builderCU -> {

                    String sourceFile = this.sourceFile.getAbsolutePath();
                    String builderClassName = sourceFile.substring(sourceFile.indexOf("src/main/java") + "src/main/java".length()).replace(constructor.getName() + ".java", "");
                    File builderDir = new File(generatedSourcesDir, builderClassName);
                    builderDir.mkdirs();
                    writeCU(new File(builderDir, constructor.getName() + buildParameters.getBuilderClassSuffix() + ".java"), builderCU);
                    processed[0] = true;
                });
            }
        }
        return processed[0];
    }

    private void writeCU(File outputFile, CompilationUnit compilationUnit) {
        try {
            PrintWriter writer = new PrintWriter(outputFile, "UTF-8");
            final DefaultPrettyPrinter defaultPrettyPrinter = new DefaultPrettyPrinter();
            writer.print(defaultPrettyPrinter.print(compilationUnit));
            writer.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private boolean containsAnnotation(final CompilationUnit cu, String annotationClassName) {
        final TypeDeclaration<?> publicClass = getPublicClassFromClassUnit(cu);
        if (publicClass == null) return false;
        final String simpleClassName = annotationClassName.substring(annotationClassName.lastIndexOf('.') + 1);
        for (final AnnotationExpr annotation : publicClass.getAnnotations()) {
            if (annotation.getNameAsString().equals(annotationClassName)) return true;
            if (annotation.getNameAsString().equals(simpleClassName) && containsImport(cu.getImports(), annotationClassName)) return true;
        }
        return false;
    }

    private Optional<StepByStepFluentBuilder> getStepByStepFluentBuilderIfExists(final CompilationUnit cu) {
        final TypeDeclaration<?> publicClass = getPublicClassFromClassUnit(cu);
        if (publicClass == null) return Optional.empty();
        final String annotationClassName = StepByStepFluentBuilder.class.getName();
        final String simpleClassName = StepByStepFluentBuilder.class.getSimpleName();
        for (final AnnotationExpr annotation : publicClass.getAnnotations()) {
            if ((annotation.getNameAsString().equals(annotationClassName)) || (annotation.getNameAsString().equals(simpleClassName) && containsImport(cu.getImports(), annotationClassName))) {
                String[] skipAsNulls = exportSkipAsNullValue(annotation);
                return of(new StepByStepFluentBuilder() {
                    @Override
                    public Class<? extends Annotation> annotationType() {
                        return StepByStepFluentBuilder.class;
                    }

                    @Override
                    public Class<?> value() {
                        return null;
                    }

                    @Override
                    public String[] skipAsNull() {
                        return skipAsNulls;
                    }
                });
            }
        }
        return empty();
    }

    @SuppressWarnings("SameParameterValue")
    private String[] exportSkipAsNullValue(AnnotationExpr annotation) {
        return Stream.of(annotation)
            .filter(a -> a instanceof NormalAnnotationExpr)
            .map(a -> ((NormalAnnotationExpr) annotation).getPairs())
            .flatMap(Collection::stream)
            .filter(pair -> pair.getNameAsString().equals("skipAsNull"))
            .map(MemberValuePair::getValue)
            .filter(o -> o instanceof ArrayInitializerExpr)
            .map(o -> ((ArrayInitializerExpr) o).getValues())
            .flatMap(Collection::stream)
            .filter(v -> v instanceof StringLiteralExpr)
            .map(v -> ((StringLiteralExpr) v).getValue())
            .toArray(String[]::new);
    }

    private boolean containsImport(final List<ImportDeclaration> imports, final String className) {
        for (final ImportDeclaration anImport : imports) {
            if (anImport.getName().toString().equals(className)) {
                return true;
            }
        }
        return false;
    }

    private Optional<CompilationUnit> process(Constructor constructor) {

        Set<String> skipAsNull = stepByStepFluentBuilderAnnotation
            .map(StepByStepFluentBuilder::skipAsNull)
            .map(Stream::of)
            .map(s -> s.collect(Collectors.toSet()))
            .orElse(Collections.emptySet());

        List<InterfaceDescription> interfaces = interfacesGenerator.getInterfaces(constructor, skipAsNull);
        InterfaceDescription namedFirstStep = interfaces.get(0);
        interfaces.set(0, new InterfaceDescription(FIRST_STEP_INTERFACE, namedFirstStep.getParameter(), namedFirstStep.getResultType(), namedFirstStep.getMethodName()));
        List<StepDescription> stepClasses = stepsClassGenerator.getClasses(interfaces);

        TypeDeclaration<?> publicClass = getPublicClassFromClassUnit(this.cu);
        if (publicClass == null || publicClass.hasModifier(Keyword.ABSTRACT)) {
            return empty();
        }

        TypeDeclaration<?> builderClass;

        CompilationUnit builderCU = new CompilationUnit();
        builderCU.setPackageDeclaration(this.cu.getPackageDeclaration().orElse(null));
        builderCU.setImports(this.cu.getImports());
        builderCU.addImport(StepByStepBuilderFor.class.getName());
        builderClass = builderCU.addClass(publicClass.getNameAsString() + buildParameters.getBuilderClassSuffix());
        ClassOrInterfaceType classOrInterfaceType = new JavaParser().parseClassOrInterfaceType(publicClass.getNameAsString()).getResult().get();
        builderClass.addAnnotation(new SingleMemberAnnotationExpr(new Name("StepByStepBuilderFor"), new ClassExpr(classOrInterfaceType)));

        mainGenerator.generate(builderClass, constructor, namedFirstStep, interfaces, stepClasses, skipAsNull);
        return Optional.of(builderCU);
    }

    private TypeDeclaration<?> getPublicClassFromClassUnit(final CompilationUnit cu) {
        return cu.getPrimaryType().orElse(null);
    }

    public static class CleanupExistingBuilder extends ModifierVisitor<Object> {
        private final Set<String> oldBuilderNames = new HashSet<>();
        private final String className;

        CleanupExistingBuilder(String className, String oldBuilderNames) {
            this.className = className;
            if (oldBuilderNames != null) {
                this.oldBuilderNames.addAll(Arrays.asList(oldBuilderNames.split(",")));
            }
        }

        @Override
        public Node visit(final ClassOrInterfaceDeclaration declarationExpr, final Object arg) {
            super.visit(declarationExpr, arg);
            if (declarationExpr.getNameAsString().equals(className)) {
                return null;
            }
            if (oldBuilderNames.contains(declarationExpr.getNameAsString())) {
                return null;
            }
            return declarationExpr;
        }


    }


}
