package pl.inpar.builder.generator.javaparser.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author drackowski
 */
public class Constructor {

    private final String name;

    private final List<ParameterWithType> parameters;

    private final List<Constructor> constructorsForSubclass = new ArrayList<>();

    public Constructor(String name, List<ParameterWithType> parameters) {
        this.name = name;
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public List<ParameterWithType> getParameters() {
        return parameters;
    }

    public List<Constructor> getConstructorsForSubclass() {
        return constructorsForSubclass;
    }

    @Override
    public String toString() {
        return "Constructor{" +
                "name='" + name + '\'' +
                ", parameters=" + parameters +
                ", constructorsForSubclass=" + constructorsForSubclass +
                '}';
    }
}
