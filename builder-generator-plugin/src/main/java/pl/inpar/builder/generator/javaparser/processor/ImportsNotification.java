package pl.inpar.builder.generator.javaparser.processor;

import com.github.javaparser.ast.body.Parameter;

/**
 * @author drackowski
 */
public interface ImportsNotification {

    void notifyUsedType(Parameter classNameOrSimpleClassName);
}
