package pl.inpar.builder.generator.javaparser.model;

/**
 * @author drackowski
 */
public class InterfaceDescription {

    private final String name;

    private final String methodName;

    private final String resultType;

    private final ParameterWithType parameter;

    public InterfaceDescription(String name, ParameterWithType parameter, String resultType, String methodName) {
        this.name = name;
        this.parameter = parameter;
        this.resultType = resultType;
        this.methodName = methodName;
    }

    public String getName() {
        return name;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getResultType() {
        return resultType;
    }

    public ParameterWithType getParameter() {
        return parameter;
    }

    @Override
    public String toString() {
        return "InterfaceDescription{" +
                "name='" + name + '\'' +
                ", methodName='" + methodName + '\'' +
                ", resultType='" + resultType + '\'' +
                ", parameter=" + parameter +
                '}';
    }
}
