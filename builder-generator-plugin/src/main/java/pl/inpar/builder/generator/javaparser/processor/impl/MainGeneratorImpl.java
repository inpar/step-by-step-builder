package pl.inpar.builder.generator.javaparser.processor.impl;

import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.Modifier.Keyword;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.AssignExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.ObjectCreationExpr;
import com.github.javaparser.ast.expr.ThisExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.ReturnStmt;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import pl.inpar.builder.BuildParameters;
import pl.inpar.builder.generator.javaparser.model.Constructor;
import pl.inpar.builder.generator.javaparser.model.InterfaceDescription;
import pl.inpar.builder.generator.javaparser.model.ParameterWithType;
import pl.inpar.builder.generator.javaparser.model.StepDescription;
import pl.inpar.builder.generator.javaparser.processor.MainGenerator;

import java.util.List;
import java.util.Set;

/**
 * @author drackowski
 */
@SuppressWarnings("Duplicates")
public class MainGeneratorImpl implements MainGenerator {
    private static final String STEPS_CLASS_NAME = "Steps";

    private final BuildParameters buildParameters;

    public MainGeneratorImpl(BuildParameters buildParameters) {
        this.buildParameters = buildParameters;
    }

    @Override
    public void generate(TypeDeclaration<?> builderClass, Constructor constructor, InterfaceDescription namedFirstStep,
                         List<InterfaceDescription> interfaces, List<StepDescription> stepClasses, Set<String> skipAsNull) {

        // step class
        ClassOrInterfaceDeclaration stepsClass = new ClassOrInterfaceDeclaration(Modifier.createModifierList(Keyword.PUBLIC, Keyword.STATIC), false, STEPS_CLASS_NAME);

        // "start" method
        ClassOrInterfaceType firstStepType = new ClassOrInterfaceType(null, namedFirstStep.getName());
        MethodDeclaration startMethod = new MethodDeclaration(Modifier.createModifierList(Keyword.PUBLIC, Keyword.STATIC), firstStepType, buildParameters.getStaticStarterMethod());
        BlockStmt startMethodBody = new BlockStmt();
        ClassOrInterfaceType type = new ClassOrInterfaceType(null, stepsClass.getNameAsString());
        startMethodBody.getStatements().add(new ReturnStmt(new ObjectCreationExpr(null, type, new NodeList<>())));
        startMethod.setBody(startMethodBody);
        builderClass.addMember(startMethod);

        // step class add fields
        for (ParameterWithType parameterWithType : constructor.getParameters()) {
            if (skipAsNull.contains(parameterWithType.getName())) {
                continue;
            }
            stepsClass.addField(new ClassOrInterfaceType(null, parameterWithType.getType()), parameterWithType.getName(), Keyword.PRIVATE);
        }

        // step class implementing of interface methods
        for (InterfaceDescription i : interfaces) {
            NodeList<Parameter> parameters = new NodeList<>();
            if (i.getParameter() != null) {
                parameters.add(new Parameter(new ClassOrInterfaceType(null, i.getParameter().getType()), i.getParameter().getName()));
            }
            MethodDeclaration methodWithBody = new MethodDeclaration(Modifier.createModifierList(Keyword.PUBLIC), i.getMethodName(), new ClassOrInterfaceType(null, i.getResultType()), parameters);
            BlockStmt setterBody = new BlockStmt();

            if (i.getMethodName().equals("build")) {
                ObjectCreationExpr objCreationExpr = new ObjectCreationExpr();
                objCreationExpr.setType(new ClassOrInterfaceType(null, i.getResultType()));
                NodeList<Expression> args = new NodeList<>();
                for (ParameterWithType parameterWithType : constructor.getParameters()) {
                    if (skipAsNull.contains(parameterWithType.getName())) {
                        args.add(new NameExpr("null"));
                    } else {
                        args.add(new NameExpr(parameterWithType.getName()));
                    }
                }
                objCreationExpr.setArguments(args);
                setterBody.addStatement(new ReturnStmt(objCreationExpr));
            } else {
                setterBody.addStatement(new AssignExpr(new FieldAccessExpr(new ThisExpr(), i.getParameter().getName()), new NameExpr(i.getParameter().getName()), AssignExpr.Operator.ASSIGN));
                setterBody.addStatement(new ReturnStmt(new ThisExpr()));
            }

            methodWithBody.setBody(setterBody);
            stepsClass.addMember(methodWithBody);

        }

        // interfaces
        ClassOrInterfaceDeclaration firstInterfaceNamedAndExtendingFirstStep = new ClassOrInterfaceDeclaration(Modifier.createModifierList(Keyword.PUBLIC), true, namedFirstStep.getName());
        firstInterfaceNamedAndExtendingFirstStep.addExtendedType(FileProcessorImpl.FIRST_STEP_INTERFACE);
        builderClass.addMember(firstInterfaceNamedAndExtendingFirstStep);

        for (InterfaceDescription interfaceDescription : interfaces) {
            ClassOrInterfaceDeclaration stepInterface = new ClassOrInterfaceDeclaration(Modifier.createModifierList(Keyword.PUBLIC), true, interfaceDescription.getName());
            NodeList<Parameter> params = new NodeList<>();
            if (interfaceDescription.getParameter() != null) {
                params.add(new Parameter(new ClassOrInterfaceType(null, interfaceDescription.getParameter().getType()), interfaceDescription.getParameter().getName()));
            }

            ClassOrInterfaceType methodParameterType = new ClassOrInterfaceType(null, interfaceDescription.getResultType());
            MethodDeclaration interfaceMethodDeclaration = new MethodDeclaration(Modifier.createModifierList(), interfaceDescription.getMethodName(), methodParameterType, params);
            interfaceMethodDeclaration.setBody(null);
            stepInterface.addMember(interfaceMethodDeclaration);
            builderClass.addMember(stepInterface);
        }

        // step class implements interfaces
        NodeList<ClassOrInterfaceType> implementsList = new NodeList<>();
        implementsList.add(new ClassOrInterfaceType(null, namedFirstStep.getName()));
        for (InterfaceDescription interfaceDescription : interfaces) {
            implementsList.add(new ClassOrInterfaceType(null, interfaceDescription.getName()));
        }
        stepsClass.setImplementedTypes(implementsList);
        builderClass.addMember(stepsClass);

    }
}
