package pl.inpar.builder.generator.javaparser.utils;

/**
 * @author drackowski
 */
public class StringUtils {

    public static String toUpperFirstLetter(String input) {
        if (input == null || input.length() < 1) return input;
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

}
