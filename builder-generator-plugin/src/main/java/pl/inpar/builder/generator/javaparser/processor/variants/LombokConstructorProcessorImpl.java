package pl.inpar.builder.generator.javaparser.processor.variants;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.AnnotationExpr;
import pl.inpar.builder.generator.javaparser.model.Constructor;
import pl.inpar.builder.generator.javaparser.model.ParameterWithType;
import pl.inpar.builder.generator.javaparser.processor.VariantsProcessor;

import java.util.ArrayList;
import java.util.List;


/**
 * @author drackowski
 */
@SuppressWarnings("Duplicates")
public class LombokConstructorProcessorImpl implements VariantsProcessor {

    enum KnownLombok {
        ALL_ARGS_CONSTRUCTOR,
        REQUIRED_ARGS_CONSTRUCTOR,

        NONE
    }

    private final CompilationUnit cu;

    public LombokConstructorProcessorImpl(CompilationUnit cu) {
        this.cu = cu;
    }

    @Override
    public List<Constructor> getBuilderVariants() {
        List<Constructor> result = new ArrayList<>();

        TypeDeclaration publicClass = cu.getPrimaryType().orElse(null);
        if (publicClass == null) {
            throw new RuntimeException("Could not find public class");
        }

        for (Object annotation : publicClass.getAnnotations()) {
            switch (checkLombokTypeOfAnnotation(annotation, cu.getImports())) {
                case ALL_ARGS_CONSTRUCTOR:
                    result.add(processAllArgsConstructor(publicClass));
                    break;
                case REQUIRED_ARGS_CONSTRUCTOR:
                    result.add(processRequiredArgsConstructor(publicClass));
                    break;
                default:
                    break;
            }
        }

        return result;
    }

    private Constructor processRequiredArgsConstructor(TypeDeclaration clazz) {
        List<ParameterWithType> parameters = new ArrayList<>();
        for (Object member : clazz.getMembers()) {
            if (member instanceof FieldDeclaration) {
                FieldDeclaration field = (FieldDeclaration) member;
                if (nonStatic(field) && nonDefinedFinalField(field)) {
                    if (field.hasModifier(Modifier.Keyword.FINAL) || hasNotNullAnnotation(field.getAnnotations())) {
                        for (VariableDeclarator variable : field.getVariables()) {
                            parameters.add(new ParameterWithType(field.getCommonType().toString(), variable.getNameAsString()));
                        }
                    }
                }
            }
        }
        return new Constructor(clazz.getNameAsString(), parameters);
    }

    private boolean hasNotNullAnnotation(List<AnnotationExpr> annotations) {
        for (AnnotationExpr annotation : annotations) {
            if (annotation.getName().toString().equals("NotNull")) return true;
        }
        return false;
    }

    private Constructor processAllArgsConstructor(TypeDeclaration clazz) {
        List<ParameterWithType> parameters = new ArrayList<>();
        for (Object member : clazz.getMembers()) {
            if (member instanceof FieldDeclaration) {
                FieldDeclaration field = (FieldDeclaration) member;
                if (nonStatic(field) && nonDefinedFinalField(field)) {
                    for (VariableDeclarator variable : field.getVariables()) {
                        parameters.add(new ParameterWithType(field.getCommonType().toString(), variable.getNameAsString()));
                    }
                }
            }
        }
        return new Constructor(clazz.getNameAsString(), parameters);
    }

    private boolean nonStatic(FieldDeclaration field) {
        return !field.hasModifier(Modifier.Keyword.STATIC);
    }

    private boolean nonDefinedFinalField(FieldDeclaration field) {
        boolean isFinal = field.hasModifier(Modifier.Keyword.FINAL);
        NodeList<VariableDeclarator> variables = field.getVariables();
        boolean isOneVariableWithInitialization = variables.size() == 1 && variables.get(0).getInitializer().isPresent();
        return !isFinal || !isOneVariableWithInitialization;
    }

    private KnownLombok checkLombokTypeOfAnnotation(Object annotation, List<ImportDeclaration> imports) {
        if (!(annotation instanceof AnnotationExpr)) {
            return KnownLombok.NONE;
        }
        String annotationName = ((AnnotationExpr)annotation).getNameAsString();
        if ("AllArgsConstructor".equals(annotationName) && isPresent(imports, "AllArgsConstructor")) {
            return KnownLombok.ALL_ARGS_CONSTRUCTOR;
        }
        if ("RequiredArgsConstructor".equals(annotationName) && isPresent(imports, "RequiredArgsConstructor")) {
            return KnownLombok.REQUIRED_ARGS_CONSTRUCTOR;
        }
        return KnownLombok.NONE;
    }

    private boolean isPresent(List<ImportDeclaration> imports, String annotation) {
        for (ImportDeclaration i : imports) {
            if (i.isAsterisk() && i.getNameAsString().equals("lombok")) return true;
            if (i.getName().toString().equals("lombok." + annotation)) return true;
        }
        return false;
    }

}
