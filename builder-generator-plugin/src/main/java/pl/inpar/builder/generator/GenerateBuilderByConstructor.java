package pl.inpar.builder.generator;


import pl.inpar.builder.BuildParameters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import static java.lang.String.format;

/**
 * @author drackowski
 */
public class GenerateBuilderByConstructor {

    private final int FIELD_TYPE = 0;
    private final int FIELD_NAME = 1;
    private final BuildParameters parameters;

    private String className;
    private String packageName;
    private List<String> importedClasses = new ArrayList<>();
    private List<String[]> fields = new ArrayList<>();

    private String builderClassName;
    private String inputPackage;
    private String outputPackage;

    public GenerateBuilderByConstructor(BuildParameters parameters) {
        this.parameters = parameters;
    }

    public GenerateOutput process(List<String> inputLines, String inputPackage, String outputPackage) throws ProcessingException {
        for (String line : inputLines) {
            if (line.trim().startsWith("package ")) {
                packageName = line.trim().split("[\\s+;]")[1];
            }
            if (line.trim().contains("class ")) {
                className = line.trim().replaceFirst(".*class", "").trim().split("\\s")[0];
            }
            if (line.trim().contains("enum ") && className == null) {
                return null;
            }
            if (line.trim().startsWith("import ")) {
                String e = line.trim().split("[\\s+;]")[1];
                if (!e.contains("*") && !e.trim().equals("static")) {
                    importedClasses.add(e);
                }
            }
            if (line.trim().startsWith("private")) {
                String fieldLine = line.trim();
                fieldLine = fieldLine.split("[;=]")[0]; //usun wszystko co jest za srednikiem lub znakiem =
                fieldLine = fieldLine.substring("primitive".length() - 1).trim();
                int spaceBeforeFieldName = fieldLine.lastIndexOf(' ');
                String fieldType = fieldLine.substring(0, spaceBeforeFieldName).trim();
                String fieldName = fieldLine.substring(spaceBeforeFieldName).trim();
                fields.add(new String[] {fieldType, fieldName});
            }
        }
        if (className == null || className.trim().isEmpty()) {
            throw new ProcessingException("Class name not found");
        }
        if (fields.isEmpty()) {
            throw new ProcessingException("Fields not found");
        }
        builderClassName = className + parameters.getBuilderClassSuffix();
        this.inputPackage = inputPackage;
        this.outputPackage = outputPackage;
        return new GenerateOutput(builderClassName, generate());
    }

    private String generate() {
        StringBuilder sb = new StringBuilder("");

        String packageSuffix = packageName.substring(inputPackage.length());
        sb.append(format("package %s;\n\n", outputPackage + packageSuffix));

        printUsedImports(sb);

        sb.append(format("public class %s {\n\n", builderClassName));

        sb.append(format("\tpublic static %s start() {\n", toStepTypeName(fields.get(0)[FIELD_NAME])));
        sb.append(format("\t\treturn new Steps();\n"));
        sb.append(format("\t}\n"));
        sb.append("\n");
        sb.append(format("\tpublic interface BuildStep { %s build(); }\n", className));
        sb.append(format("\t\n"));

        for (int i = 0; i < fields.size(); i++) {
            String[] currentField = fields.get(i);
            String[] nextField = i + 1 < fields.size() ? fields.get(i + 1) : new String[]{className, "build"};

            String currentName = currentField[FIELD_NAME];
            String currentType = currentField[FIELD_TYPE];
            String currentStepType = toStepTypeName(currentName);
            String nextStepType = toStepTypeName(nextField[FIELD_NAME]);

            sb.append(format("\tpublic interface %s { %s %s(%s %s); }\n", currentStepType, nextStepType, setter(currentName), currentType, currentName));
        }

        List<String> stepTypeNames = new ArrayList<String>();
        for (String[] field : fields) {
            stepTypeNames.add(toStepTypeName(field[FIELD_NAME]));
        }
        stepTypeNames.add("BuildStep");
        sb.append(format("\tpublic static class Steps implements %s {\n", join(", ", stepTypeNames)));

        for (int i = 0; i < fields.size(); i++) {
            String[] currentField = fields.get(i);
            String currentName = currentField[FIELD_NAME];
            String currentType = currentField[FIELD_TYPE];

            sb.append(format("\t\tprivate %s %s;\n", currentType, currentName));
        }
        sb.append(format("\t\n"));

        for (int i = 0; i < fields.size(); i++) {
            String[] currentField = fields.get(i);
            String[] nextField = i + 1 < fields.size() ? fields.get(i + 1) : new String[]{className, "build"};

            String currentName = currentField[FIELD_NAME];
            String currentType = currentField[FIELD_TYPE];
            String nextStepType = toStepTypeName(nextField[FIELD_NAME]);

            sb.append(format("\t\tpublic %s %s(%s %s) {\n", nextStepType, setter(currentName), currentType, currentName));
            sb.append(format("\t\t\tthis.%s = %s;\n", currentName, currentName));
            sb.append(format("\t\t\treturn this;\n"));
            sb.append(format("\t\t}\n"));
            sb.append(format("\n"));
        }

        List<String> fieldNames = new ArrayList<String>();
        for (String[] field : fields) {
            fieldNames.add(field[FIELD_NAME]);
        }
        sb.append(format("\t\tpublic %s build() {\n", className));
        sb.append(format("\t\t\treturn new %s(%s);\n", className, join(", ", fieldNames)));
        sb.append(format("\t\t}\n"));
        sb.append(format("\n"));


        sb.append(format("\t}\n"));

        sb.append(format("}\n"));

        return sb.toString();
    }

    private void printUsedImports(StringBuilder sb) {
        Map<String, String> map = new HashMap<>();
        for (String i : importedClasses) {
            String clazz = i.substring(i.lastIndexOf('.') + 1);
            map.put(clazz, i);
        }
        final TreeSet<String> classToImports = new TreeSet<>();
        for (String[] field : fields) {
            String fieldType = field[FIELD_TYPE];
            for (String typeToImport : map.keySet()) {
                if ((" " + fieldType + " ").matches(".*[^a-zA-Z]" + typeToImport + "[^a-zA-Z].*")){
                    classToImports.add(map.get(typeToImport));
                }
            }
        }
        for (String classToImport : classToImports) {
            sb.append(format("import %s;\n", classToImport));
        }
        if (!packageName.equals(outputPackage)) {
            sb.append(format("import %s;\n", packageName + "." + className));
        }
        sb.append("\n");
    }

    private String setter(String fieldName) {
        switch (parameters.getSetterType()) {
            case FIELD_NAME:
                return fieldName;
            case SETTER:
                return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            case WITH:
                return "with" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            default:
                return fieldName;
        }
    }

    private String toStepTypeName(String fieldName) {
        return fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1) + "Step";
    }

    private String join(String delimiter, Collection<String> elements) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = elements.iterator();
        if (it.hasNext()) {
            sb.append(it.next());
            while (it.hasNext()) {
                sb.append(delimiter).append(it.next());
            }
        }
        return sb.toString();
    }

}
