package pl.inpar.builder.generator.javaparser.processor;

import java.io.File;
import java.util.List;

/**
 * @author drackowski
 */
public interface InputFileScanner {

    List<File> findFilesToProcess();

}
