package pl.inpar.builder.generator.javaparser.processor;

import pl.inpar.builder.generator.javaparser.model.ParameterWithType;

/**
 * @author drackowski
 */
public interface ParameterWithTypeToStepClassName {
    String convert(ParameterWithType parameterWithType);
}
