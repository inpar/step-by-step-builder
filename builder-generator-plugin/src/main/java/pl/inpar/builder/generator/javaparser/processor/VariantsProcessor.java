package pl.inpar.builder.generator.javaparser.processor;

import pl.inpar.builder.generator.javaparser.model.Constructor;

import java.util.List;

/**
 * @author drackowski
 */
public interface VariantsProcessor {

    List<Constructor> getBuilderVariants();
}
