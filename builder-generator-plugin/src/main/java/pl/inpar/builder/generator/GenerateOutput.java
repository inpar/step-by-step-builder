package pl.inpar.builder.generator;

/**
 * @author drackowski
 */
public class GenerateOutput {

    private String builderClassName;

    private String body;

    public GenerateOutput(String builderClassName, String body) {
        this.builderClassName = builderClassName;
        this.body = body;
    }

    public String getBuilderClassName() {
        return builderClassName;
    }

    public void setBuilderClassName(String builderClassName) {
        this.builderClassName = builderClassName;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
