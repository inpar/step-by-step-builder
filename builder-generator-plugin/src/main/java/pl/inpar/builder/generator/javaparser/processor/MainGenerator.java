package pl.inpar.builder.generator.javaparser.processor;

import com.github.javaparser.ast.body.TypeDeclaration;
import pl.inpar.builder.generator.javaparser.model.Constructor;
import pl.inpar.builder.generator.javaparser.model.InterfaceDescription;
import pl.inpar.builder.generator.javaparser.model.StepDescription;

import java.util.List;
import java.util.Set;

/**
 * @author drackowski
 */
public interface MainGenerator {

    void generate(TypeDeclaration<?> builderClass, Constructor constructor, InterfaceDescription namedFirstStep, List<InterfaceDescription> interfaces, List<StepDescription> stepClasses, Set<String> skipAsNull);
}
