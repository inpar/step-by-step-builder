package pl.inpar.builder.generator.javaparser.processor;

import com.github.javaparser.ParseException;

import java.io.IOException;

/**
 * @author drackowski
 */
public interface BuilderProcessor {

    void process() throws IOException, ParseException;
}
