package pl.inpar.builder.generator.javaparser.processor.impl;

import pl.inpar.builder.BuildParameters;
import pl.inpar.builder.generator.javaparser.processor.BuilderProcessor;
import pl.inpar.builder.generator.javaparser.processor.InputFileScanner;

import java.io.File;
import java.io.IOException;

/**
 * @author drackowski
 */
public class BuilderProcessorImpl implements BuilderProcessor {

    private final BuildParameters buildParameters;

    public BuilderProcessorImpl(BuildParameters buildParameters) {
        this.buildParameters = buildParameters;
    }

    @Override
    public void process() throws IOException {
        InputFileScanner inputFileScanner = new InputFileScannerImpl();
        for (File file : inputFileScanner.findFilesToProcess()) {
            FileProcessorImpl fileProcessor = new FileProcessorImpl(buildParameters, file, null);
            fileProcessor.process();
        }
    }
}
