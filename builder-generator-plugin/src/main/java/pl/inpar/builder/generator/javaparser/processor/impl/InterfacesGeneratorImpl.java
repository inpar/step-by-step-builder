package pl.inpar.builder.generator.javaparser.processor.impl;

import pl.inpar.builder.BuildParameters;
import pl.inpar.builder.generator.javaparser.model.Constructor;
import pl.inpar.builder.generator.javaparser.model.InterfaceDescription;
import pl.inpar.builder.generator.javaparser.model.ParameterWithType;
import pl.inpar.builder.generator.javaparser.processor.InterfacesGenerator;
import pl.inpar.builder.generator.javaparser.processor.ParameterWithTypeToStepClassName;
import pl.inpar.builder.generator.javaparser.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author drackowski
 */
public class InterfacesGeneratorImpl implements InterfacesGenerator {

    private final BuildParameters.SetterType setterType;
    private final ParameterWithTypeToStepClassName parameterWithTypeToStepClassName;

    public InterfacesGeneratorImpl(BuildParameters.SetterType setterType, ParameterWithTypeToStepClassName parameterWithTypeToStepClassName) {
        this.setterType = setterType;
        this.parameterWithTypeToStepClassName = parameterWithTypeToStepClassName;
    }

    @Override
    public List<InterfaceDescription> getInterfaces(Constructor constructor, Set<String> skipAsNull) {
        List<InterfaceDescription> result = new ArrayList<>();
        List<ParameterWithType> parameterWithTypes = constructor.getParameters().stream().filter(p -> !skipAsNull.contains(p.getName())).collect(Collectors.toList());
        parameterWithTypes.add(null);
        for (int i = 0; i < parameterWithTypes.size() - 1; i++) {
            ParameterWithType currentParameter = parameterWithTypes.get(i);
            ParameterWithType nextParameter = parameterWithTypes.get(i + 1);

            String setterName = setter(setterType, currentParameter.getName());

            String nextStepType;
            if (nextParameter != null) {
                nextStepType = parameterWithTypeToStepClassName.convert(nextParameter);
            } else {
                nextStepType = parameterWithTypeToStepClassName.convert(new ParameterWithType(constructor.getName(), "build"));
            }

            result.add(new InterfaceDescription(parameterWithTypeToStepClassName.convert(currentParameter), currentParameter, nextStepType, setterName));
        }
        result.add(new InterfaceDescription(parameterWithTypeToStepClassName.convert(new ParameterWithType(constructor.getName(), "build")), null, constructor.getName(), "build"));
        return result;
    }

    private String setter(BuildParameters.SetterType setterType, String fieldName) {
        switch (setterType) {
            case SETTER:
                return "set" + StringUtils.toUpperFirstLetter(fieldName);
            case WITH:
                return "with" + StringUtils.toUpperFirstLetter(fieldName);
            case FIELD_NAME:
            default:
                return fieldName;
        }
    }

}
