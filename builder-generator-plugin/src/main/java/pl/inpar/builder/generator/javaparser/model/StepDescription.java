package pl.inpar.builder.generator.javaparser.model;

/**
 * @author drackowski
 */
public class StepDescription {

    private final InterfaceDescription interfaceDescription;

    public StepDescription(InterfaceDescription interfaceDescription) {
        this.interfaceDescription = interfaceDescription;
    }

    public InterfaceDescription getInterfaceDescription() {
        return interfaceDescription;
    }

    @Override
    public String toString() {
        return "StepDescription{" +
                "interfaceDescription=" + interfaceDescription +
                '}';
    }
}
