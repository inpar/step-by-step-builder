package pl.inpar.builder.generator.javaparser.processor.impl;

import pl.inpar.builder.generator.javaparser.model.InterfaceDescription;
import pl.inpar.builder.generator.javaparser.model.StepDescription;
import pl.inpar.builder.generator.javaparser.processor.StepsClassGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author drackowski
 */
public class StepsClassGeneratorImpl implements StepsClassGenerator {

    @Override
    public List<StepDescription> getClasses(List<InterfaceDescription> interfaces) {
        List<StepDescription> result = new ArrayList<>();
        for (InterfaceDescription interfaceDescription : interfaces) {
            result.add(new StepDescription(interfaceDescription));
        }
        return result;
    }
}
