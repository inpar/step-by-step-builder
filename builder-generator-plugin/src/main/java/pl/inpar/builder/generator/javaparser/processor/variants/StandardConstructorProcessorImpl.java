package pl.inpar.builder.generator.javaparser.processor.variants;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.body.TypeDeclaration;
import pl.inpar.builder.generator.javaparser.model.Constructor;
import pl.inpar.builder.generator.javaparser.model.ParameterWithType;
import pl.inpar.builder.generator.javaparser.processor.VariantsProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author drackowski
 */
@SuppressWarnings("Duplicates")
public class StandardConstructorProcessorImpl implements VariantsProcessor {

    private final CompilationUnit cu;

    public StandardConstructorProcessorImpl(CompilationUnit cu) {
        this.cu = cu;
    }

    @Override
    public List<Constructor> getBuilderVariants() {
        List<Constructor> result = new ArrayList<>();

        TypeDeclaration publicClass = null;
        for (TypeDeclaration typeDeclaration : cu.getTypes()) {
            if (typeDeclaration.hasModifier(Modifier.Keyword.PUBLIC)) {
                publicClass = typeDeclaration;
            }
        }

        if (publicClass == null) return new ArrayList<>();

        for (Object m : publicClass.getMembers()) {
            if (m instanceof ConstructorDeclaration) {
                // mamy konstruktor
                ConstructorDeclaration constructorDeclaration = (ConstructorDeclaration) m;
                if (constructorDeclaration.getParameters().size() == 0) {
                    continue;
                }

                // dla znalezionego konstruktora generujemy listę deklaracji pól dla buildera
                List<ParameterWithType> parameters = new ArrayList<>();
                for (Parameter parameter : constructorDeclaration.getParameters()) {
                    parameters.add(new ParameterWithType(parameter.getType().toString(), parameter.getNameAsString()));
                }

                result.add(new Constructor(constructorDeclaration.getNameAsString(), parameters));
            }
        }

        return result;
    }
}
