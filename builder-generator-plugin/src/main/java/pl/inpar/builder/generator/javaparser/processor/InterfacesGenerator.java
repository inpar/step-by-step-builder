package pl.inpar.builder.generator.javaparser.processor;

import pl.inpar.builder.generator.javaparser.model.Constructor;
import pl.inpar.builder.generator.javaparser.model.InterfaceDescription;

import java.util.List;
import java.util.Set;

/**
 * @author drackowski
 */
public interface InterfacesGenerator {

    List<InterfaceDescription> getInterfaces(Constructor constructor, Set<String> strings);
}
