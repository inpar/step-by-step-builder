package pl.inpar.builder.generator.javaparser.processor;

import pl.inpar.builder.generator.javaparser.model.InterfaceDescription;
import pl.inpar.builder.generator.javaparser.model.StepDescription;

import java.util.List;

/**
 * @author drackowski
 */
public interface StepsClassGenerator {

    List<StepDescription> getClasses(List<InterfaceDescription> interfaces);
}
