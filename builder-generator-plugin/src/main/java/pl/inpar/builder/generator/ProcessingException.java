package pl.inpar.builder.generator;

/**
 * @author drackowski
 */
public class ProcessingException extends Exception {

    public ProcessingException(String message) {
        super(message);
    }
}
