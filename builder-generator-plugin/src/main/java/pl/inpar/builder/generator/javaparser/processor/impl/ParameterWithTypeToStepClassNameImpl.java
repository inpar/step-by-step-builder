package pl.inpar.builder.generator.javaparser.processor.impl;

import pl.inpar.builder.generator.javaparser.model.ParameterWithType;
import pl.inpar.builder.generator.javaparser.processor.ParameterWithTypeToStepClassName;
import pl.inpar.builder.generator.javaparser.utils.StringUtils;

/**
 * @author drackowski
 */
public class ParameterWithTypeToStepClassNameImpl implements ParameterWithTypeToStepClassName {
    @Override
    public String convert(ParameterWithType parameterWithType) {
        return "Step" + StringUtils.toUpperFirstLetter(parameterWithType.getName());
    }
}
