package pl.inpar.builder.generator.javaparser.model;

/**
 * @author drackowski
 */
public class ParameterWithType {

    private final String type;
    private final String name;

    public ParameterWithType(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ParameterWithType{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
