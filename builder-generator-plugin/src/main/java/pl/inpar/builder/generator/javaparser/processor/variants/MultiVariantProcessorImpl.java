package pl.inpar.builder.generator.javaparser.processor.variants;

import pl.inpar.builder.generator.javaparser.model.Constructor;
import pl.inpar.builder.generator.javaparser.processor.VariantsProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author drackowski
 */
public class MultiVariantProcessorImpl implements VariantsProcessor {

    private final VariantsProcessor[] processors;

    public MultiVariantProcessorImpl(VariantsProcessor... processors) {
        this.processors = processors;
    }

    @Override
    public List<Constructor> getBuilderVariants() {
        List<Constructor> result = new ArrayList<>();
        for (VariantsProcessor processor : processors) {
            result.addAll(processor.getBuilderVariants());
        }
        return result;
    }
}
