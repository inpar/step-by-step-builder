package pl.inpar.builder.generator.javaparser.processor.variants;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.body.RecordDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import pl.inpar.builder.generator.javaparser.model.Constructor;
import pl.inpar.builder.generator.javaparser.model.ParameterWithType;
import pl.inpar.builder.generator.javaparser.processor.VariantsProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author drackowski
 */
@SuppressWarnings("Duplicates")
public class RecordProcessorImpl implements VariantsProcessor {

    private final CompilationUnit cu;

    public RecordProcessorImpl(CompilationUnit cu) {
        this.cu = cu;
    }

    @Override
    public List<Constructor> getBuilderVariants() {
        List<Constructor> result = new ArrayList<>();

        RecordDeclaration record = null;
        for (TypeDeclaration<?> typeDeclaration : cu.getTypes()) {
            if (typeDeclaration.hasModifier(Modifier.Keyword.PUBLIC) && typeDeclaration instanceof RecordDeclaration) {
                record = (RecordDeclaration) typeDeclaration;
            }
        }

        if (record == null) return new ArrayList<>();


        List<ParameterWithType> parameters = new ArrayList<>();
        for (Parameter parameter : record.getParameters()) {
            parameters.add(new ParameterWithType(parameter.getType().toString(), parameter.getNameAsString()));
        }

        result.add(new Constructor(record.getNameAsString(), parameters));

        return result;
    }
}
