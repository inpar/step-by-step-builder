package pl.inpar.builder.generator.javaparser.processor;

import com.github.javaparser.ast.CompilationUnit;

/**
 * @author drackowski
 */
public interface ResultToFileWriter {

    void writeToFile(CompilationUnit body);
}
