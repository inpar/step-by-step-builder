package pl.inpar.builder;


import pl.inpar.builder.generator.javaparser.processor.impl.FileProcessorImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.io.FileUtils.iterateFiles;

/**
 * @author drackowski
 */
public class BuilderGenerator {
    private static final int EXECUTOR_THREADS = 32;

    private final BuildParameters builderParameters;
    private final List<String> sourceDirList;
    private final File generatedSourcesDir;
    private final Logger logger;

    public interface Logger {
        void log(String text);
    }

    public BuilderGenerator(BuildParameters builderParameters, List<String> sourceDirList, File generatedSourcesDir, Logger logger) {
        this.builderParameters = builderParameters;
        this.sourceDirList = sourceDirList;
        this.generatedSourcesDir = generatedSourcesDir;
        this.logger = logger;
    }

    public void start() {
        int count = 0;
        final ExecutorService executor = Executors.newFixedThreadPool(EXECUTOR_THREADS);
        final List<Future<Boolean>> processFileResults = new ArrayList<>();
        for (String srcDir : sourceDirList) {
            File inputDir = new File(srcDir);
            if (inputDir.exists()) {
                Iterator<File> sourcesIterator = iterateFiles(inputDir, new String[]{"java"}, true);
                while (sourcesIterator.hasNext()) {
                    File sourceFile = sourcesIterator.next();
                    processFileResults.add(executor.submit(() -> processFile(sourceFile)));
                }
            }
        }
        for (Future<Boolean> processFileResult : processFileResults) {
            try {
                final Boolean result = processFileResult.get();
                if (result) {
                    count++;
                }
            } catch (InterruptedException | ExecutionException e) {
                logger.log("[ERROR] Could not get result of file processing: " + e.getMessage());
            }
        }
        executor.shutdown();
        try {
            executor.awaitTermination(30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            logger.log("[ERROR] Timeout of awaiting termination: " + e.getMessage());
        }
        logger.log("Processed files: " + count);
    }

    private boolean processFile(File sourceFile) {
        try {
            FileProcessorImpl fileProcessor = new FileProcessorImpl(builderParameters, sourceFile, this.generatedSourcesDir);
            final boolean process = fileProcessor.process();
            logger.log("[PROCESSED] File: " + sourceFile + " (processed: " + process + ")");
            return process;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private String getFullClassName(String src, File file, String extension) {
        String result = file.getAbsolutePath().replace(new File(src).getAbsolutePath(), "");
        if (result.endsWith(extension)) {
            result = result.substring(0, result.length() - ("." + extension).length());
        }
        if (result.charAt(0) == File.separator.charAt(0)) {
            result = result.substring(1);
        }
        return result.replace(File.separator, ".");
    }

    private String packageToPath(String pkg) {
        return pkg.replace(".", File.separator);
    }


}
