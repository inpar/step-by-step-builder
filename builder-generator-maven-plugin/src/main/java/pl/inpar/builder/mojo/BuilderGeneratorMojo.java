package pl.inpar.builder.mojo;

import pl.inpar.builder.BuildParameters;
import pl.inpar.builder.BuilderGenerator;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;

/**
 * @author drackowski
 */
@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class BuilderGeneratorMojo extends AbstractMojo {

    @Parameter
    private final BuilderParametersMojo builderParameters = new BuilderParametersMojo();

    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    private final BuilderGenerator.Logger logger = text -> getLog().info(text);

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void execute() throws MojoExecutionException {
        try {
            File generatedSourcesDir = new File(project.getBuild().getDirectory() + "/generated-sources");
            generatedSourcesDir.mkdirs();
            final BuildParameters params = new BuildParameters(builderParameters.getSetterType(), builderParameters.getBuilderClassSuffix(), builderParameters.getStaticStarterMethod());
            new BuilderGenerator(params, project.getCompileSourceRoots(), generatedSourcesDir, logger).start();
            project.addCompileSourceRoot(generatedSourcesDir.getPath());
        } catch (Exception e) {
            getLog().info("ERROR: " + e.getMessage());
            throw new MojoExecutionException("Error while executing", e);
        }
    }

}
