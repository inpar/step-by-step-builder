package pl.inpar.builder.mojo;

import pl.inpar.builder.BuildParameters.SetterType;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Plugin configuration parameters
 *
 * @author drackowski
 */
public class BuilderParametersMojo {

    @Parameter
    private SetterType setterType = SetterType.FIELD_NAME;

    @Parameter
    private String builderClassSuffix = "FluentBuilder";

    @Parameter
    private String staticStarterMethod = "start";

    public BuilderParametersMojo() {
    }

    public BuilderParametersMojo(SetterType setterType, String builderClassSuffix, String staticStarterMethod) {
        this.setterType = setterType;
        this.builderClassSuffix = builderClassSuffix;
        this.staticStarterMethod = staticStarterMethod;
    }

    public SetterType getSetterType() {
        return setterType;
    }

    public void setSetterType(SetterType setterType) {
        this.setterType = setterType;
    }

    public String getBuilderClassSuffix() {
        return builderClassSuffix;
    }

    public void setBuilderClassSuffix(String builderClassSuffix) {
        this.builderClassSuffix = builderClassSuffix;
    }

    public String getStaticStarterMethod() {
        return staticStarterMethod;
    }

    public void setStaticStarterMethod(String staticStarterMethod) {
        this.staticStarterMethod = staticStarterMethod;
    }

}